using System;
using System.Collections.Generic;
using System.Text;

namespace DBFunctions
{

    public class DBFunctions
    {
        private string sqlConnectionString = "";
        private System.Data.OleDb.OleDbConnection sqlConnection = null;
        private System.Data.OleDb.OleDbCommand sqlCommand = null;
        private string eventLogSource = "ConvertPRMArchiveToNewsML";
        private string errorMessage = "";

        public DBFunctions()
        {
            System.Configuration.AppSettingsReader applicationConfiguration = new System.Configuration.AppSettingsReader();
            eventLogSource = Convert.ToString(applicationConfiguration.GetValue("ApplicationName", string.Empty.GetType()));
            sqlConnectionString = Convert.ToString(applicationConfiguration.GetValue("SQL_Connection_String", string.Empty.GetType()));
            sqlConnection = new System.Data.OleDb.OleDbConnection(sqlConnectionString);
        }

        private bool openSQLConnection(string callingFunctionName)
        {
            string thisFunctionName = "DBFunctions.openSQLConnection()";
            if (sqlConnection.State != System.Data.ConnectionState.Open)
            {
                try
                {
                    sqlConnection.Open();
                }
                catch (System.Data.OleDb.OleDbException sqlException)
                {
                    errorMessage = "Error occured in " + thisFunctionName + " while opening connection to database, connection string:\r\n" + sqlConnectionString + "\r\nException: " + sqlException.Message + "\r\nIn function " + callingFunctionName;
                    System.Diagnostics.EventLog.WriteEntry(eventLogSource, errorMessage, System.Diagnostics.EventLogEntryType.Error);
                    if (sqlException.InnerException != null)
                    {
                        throw new System.Exception(errorMessage, sqlException.InnerException);
                    }
                    else
                    {
                        throw new System.Exception(errorMessage);
                    }
                }
                catch (System.Exception generalException)
                {
                    errorMessage = "Error occured in " + thisFunctionName + " while opening connection to database, connection string:\r\n" + sqlConnectionString + "\r\nException: " + generalException.Message + "\r\nIn function " + callingFunctionName;
                    System.Diagnostics.EventLog.WriteEntry(eventLogSource, errorMessage, System.Diagnostics.EventLogEntryType.Error);
                    if (generalException.InnerException != null)
                    {
                        throw new System.Exception(errorMessage, generalException.InnerException);
                    }
                    else
                    {
                        throw new System.Exception(errorMessage);
                    }
                }
            }

            if (sqlConnection.State == System.Data.ConnectionState.Open)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private System.Data.DataSet fillDataSet(string sqlQuery, string callingFunctionName)
        {
            string thisFunctionName = "DBFunctions.fillDataSet()";
            System.Data.DataSet sqlDataset = new System.Data.DataSet();
            sqlCommand = new System.Data.OleDb.OleDbCommand(sqlQuery, sqlConnection);
            System.Data.OleDb.OleDbDataAdapter sqlAdapter = new System.Data.OleDb.OleDbDataAdapter(sqlCommand);
            try
            {
                sqlAdapter.Fill(sqlDataset);
            }
            catch (System.Data.OleDb.OleDbException sqlException)
            {
                errorMessage = "Error occured in " + thisFunctionName + " while retrieving data from database, query:\r\n" + sqlQuery + "\r\nException: " + sqlException.Message + "\r\nCalling function " + callingFunctionName;
                System.Diagnostics.EventLog.WriteEntry(eventLogSource, errorMessage, System.Diagnostics.EventLogEntryType.Error);
                if (sqlException.InnerException != null)
                {
                    throw new System.Exception(errorMessage, sqlException.InnerException);
                }
                else
                {
                    throw new System.Exception(errorMessage);
                }
            }
            catch (System.Exception generalException)
            {
                errorMessage = "Error occured in " + thisFunctionName + " while retrieving data from database, query:\r\n" + sqlQuery + "\r\nException: " + generalException.Message + "\r\nCalling function " + callingFunctionName;
                System.Diagnostics.EventLog.WriteEntry(eventLogSource, errorMessage, System.Diagnostics.EventLogEntryType.Error);
                if (generalException.InnerException != null)
                {
                    throw new System.Exception(errorMessage, generalException.InnerException);
                }
                else
                {
                    throw new System.Exception(errorMessage);
                }
            }
            return sqlDataset;
        }

        public string buildPRMArchiveSelectionQuery(System.DateTime fromDate, System.DateTime toDate, string companyFilter)
        {
            string thisFunctionName = "DBFunctions.buildPRMArchiveSelectionQuery()";
            string sqlQuery = "select ArticleXML, CreationDateTime, ArticleTitle from articles"; //_with_subrel";
            sqlQuery += " where";
            sqlQuery += " articletitle like 'PRM:%'";
            sqlQuery += " and NTB_ID not like 'BWI:%'";
            if (fromDate != null)
            {
                sqlQuery += " and creationdatetime >= Convert(datetime, '" + fromDate.Year.ToString() + "-" + fromDate.Month.ToString() + "-" + fromDate.Day.ToString() + "')";
            }
            if (toDate != null)
            {
                sqlQuery += " and creationdatetime < Convert(datetime, '" + toDate.AddDays(1).Year.ToString() + "-" + toDate.AddDays(1).Month.ToString() + "-" + toDate.AddDays(1).Day.ToString() + "')";
            }
            if (companyFilter != "")
            {
                sqlQuery += " and ArticleXML like '%<meta name=\"NTBKilde\" content=\"" + companyFilter + "%'";
            }
            sqlQuery += " order by creationdatetime asc";
            return sqlQuery;
        }

        public string buildPRMArchiveSelectionCountQuery(System.DateTime fromDate, System.DateTime toDate, string companyFilter)
        {
            string thisFunctionName = "DBFunctions.buildPRMArchiveSelectionCountQuery()";
            string sqlQuery = "select count(refid) from articles"; //_with_subrel";
            sqlQuery += " where";
            sqlQuery += " articletitle like 'PRM:%'";
            sqlQuery += " and NTB_ID not like 'BWI:%'";
            if (fromDate != null)
            {
                sqlQuery += " and creationdatetime >= Convert(datetime, '" + fromDate.Year.ToString() + "-" + fromDate.Month.ToString() + "-" + fromDate.Day.ToString() + "')";
            }
            if (toDate != null)
            {
                sqlQuery += " and creationdatetime < Convert(datetime, '" + toDate.AddDays(1).Year.ToString() + "-" + toDate.AddDays(1).Month.ToString() + "-" + toDate.AddDays(1).Day.ToString() + "')";
            }
            if (companyFilter != "")
            {
                sqlQuery += " and ArticleXML like '%<meta name=\"NTBKilde\" content=\"" + companyFilter + "%'";
            }
            return sqlQuery;
        }

        public System.Data.DataSet getSQLDataSet(string sqlQuery)
        {
            string thisFunctionName = "DBFunctions.getSQLDataSet()";
            try
            {
                if (openSQLConnection(thisFunctionName))
                {
                    try
                    {
                        return fillDataSet(sqlQuery, thisFunctionName);
                    }
                    catch (System.Exception generalException)
                    {
                        throw generalException;
                    }
                }
                else
                {
                    return null;
                }
            }
            catch (System.Exception generalException)
            {
                throw generalException;
            }
            finally
            {
                try
                {
                    sqlConnection.Close();
                }
                catch
                {
                }
            }
        }

        public System.Data.OleDb.OleDbDataReader getSQLReader(string sqlQuery)
        {
            string thisFunctionName = "DBFunctions.getSQLReader()";
            try
            {
                if (openSQLConnection(thisFunctionName))
                {
                    try
                    {
                        sqlCommand = new System.Data.OleDb.OleDbCommand(sqlQuery, sqlConnection);
                        System.Data.OleDb.OleDbDataReader sqlReader = sqlCommand.ExecuteReader();
                        return sqlReader;
                    }
                    catch (System.Data.OleDb.OleDbException sqlException)
                    {
                        errorMessage = "Error occured in " + thisFunctionName + " while establishing datareader to database, query:\r\n" + sqlQuery + "\r\nException: " + sqlException.Message;
                        System.Diagnostics.EventLog.WriteEntry(eventLogSource, errorMessage, System.Diagnostics.EventLogEntryType.Error);
                        if (sqlException.InnerException != null)
                        {
                            throw new System.Exception(errorMessage, sqlException.InnerException);
                        }
                        else
                        {
                            throw new System.Exception(errorMessage);
                        }
                    }
                    catch (System.Exception generalException)
                    {
                        errorMessage = "Error occured in " + thisFunctionName + " while establishing datareader to database, query:\r\n" + sqlQuery + "\r\nException: " + generalException.Message;
                        System.Diagnostics.EventLog.WriteEntry(eventLogSource, errorMessage, System.Diagnostics.EventLogEntryType.Error);
                        if (generalException.InnerException != null)
                        {
                            throw new System.Exception(errorMessage, generalException.InnerException);
                        }
                        else
                        {
                            throw new System.Exception(errorMessage);
                        }
                    }
                }
                else
                {
                    return null;
                }
            }
            catch (System.Exception generalException)
            {
                throw generalException;
            }
        }
    }
}
