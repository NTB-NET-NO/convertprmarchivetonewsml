namespace ConvertPRMArchiveToNewsML
{
    partial class frmPRMToNewsML
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.archiveFrom = new System.Windows.Forms.DateTimePicker();
            this.lblArchiveFrom = new System.Windows.Forms.Label();
            this.lblArchiveTo = new System.Windows.Forms.Label();
            this.archiveTo = new System.Windows.Forms.DateTimePicker();
            this.btnConvert = new System.Windows.Forms.Button();
            this.previewData = new System.Windows.Forms.DataGridView();
            this.txtFilePathExport = new System.Windows.Forms.TextBox();
            this.lblFilePathExport = new System.Windows.Forms.Label();
            this.findExportPath = new System.Windows.Forms.FolderBrowserDialog();
            this.btnFindExportPath = new System.Windows.Forms.Button();
            this.chkShowResults = new System.Windows.Forms.CheckBox();
            this.lblNumRowsFoundLabel = new System.Windows.Forms.Label();
            this.lblNumRowsFound = new System.Windows.Forms.Label();
            this.progressBarConversion = new System.Windows.Forms.ProgressBar();
            this.lblCompanyFilter = new System.Windows.Forms.Label();
            this.txtCompanyFilter = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.previewData)).BeginInit();
            this.SuspendLayout();
            // 
            // archiveFrom
            // 
            this.archiveFrom.Location = new System.Drawing.Point(12, 25);
            this.archiveFrom.Name = "archiveFrom";
            this.archiveFrom.Size = new System.Drawing.Size(200, 20);
            this.archiveFrom.TabIndex = 1;
            this.archiveFrom.CloseUp += new System.EventHandler(this.archiveFrom_CloseUp);
            // 
            // lblArchiveFrom
            // 
            this.lblArchiveFrom.AutoSize = true;
            this.lblArchiveFrom.Location = new System.Drawing.Point(9, 9);
            this.lblArchiveFrom.Name = "lblArchiveFrom";
            this.lblArchiveFrom.Size = new System.Drawing.Size(153, 13);
            this.lblArchiveFrom.TabIndex = 2;
            this.lblArchiveFrom.Text = "Arkivdata opprettet fra og med:";
            // 
            // lblArchiveTo
            // 
            this.lblArchiveTo.AutoSize = true;
            this.lblArchiveTo.Location = new System.Drawing.Point(215, 9);
            this.lblArchiveTo.Name = "lblArchiveTo";
            this.lblArchiveTo.Size = new System.Drawing.Size(148, 13);
            this.lblArchiveTo.TabIndex = 3;
            this.lblArchiveTo.Text = "Arkivdata opprettet til og med:";
            // 
            // archiveTo
            // 
            this.archiveTo.Location = new System.Drawing.Point(218, 25);
            this.archiveTo.Name = "archiveTo";
            this.archiveTo.Size = new System.Drawing.Size(200, 20);
            this.archiveTo.TabIndex = 4;
            this.archiveTo.CloseUp += new System.EventHandler(this.archiveTo_CloseUp);
            // 
            // btnConvert
            // 
            this.btnConvert.Location = new System.Drawing.Point(424, 24);
            this.btnConvert.Name = "btnConvert";
            this.btnConvert.Size = new System.Drawing.Size(75, 23);
            this.btnConvert.TabIndex = 5;
            this.btnConvert.Text = "Konverter";
            this.btnConvert.UseVisualStyleBackColor = true;
            this.btnConvert.Click += new System.EventHandler(this.btnConvert_Click);
            // 
            // previewData
            // 
            this.previewData.AllowUserToAddRows = false;
            this.previewData.AllowUserToDeleteRows = false;
            this.previewData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.previewData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.previewData.Location = new System.Drawing.Point(13, 137);
            this.previewData.Name = "previewData";
            this.previewData.ReadOnly = true;
            this.previewData.Size = new System.Drawing.Size(485, 0);
            this.previewData.TabIndex = 6;
            this.previewData.Visible = false;
            this.previewData.SelectionChanged += new System.EventHandler(this.previewData_SelectionChanged);
            // 
            // txtFilePathExport
            // 
            this.txtFilePathExport.Location = new System.Drawing.Point(13, 72);
            this.txtFilePathExport.Name = "txtFilePathExport";
            this.txtFilePathExport.Size = new System.Drawing.Size(405, 20);
            this.txtFilePathExport.TabIndex = 7;
            // 
            // lblFilePathExport
            // 
            this.lblFilePathExport.AutoSize = true;
            this.lblFilePathExport.Location = new System.Drawing.Point(12, 56);
            this.lblFilePathExport.Name = "lblFilePathExport";
            this.lblFilePathExport.Size = new System.Drawing.Size(93, 13);
            this.lblFilePathExport.TabIndex = 8;
            this.lblFilePathExport.Text = "Filomr�de eksport:";
            // 
            // findExportPath
            // 
            this.findExportPath.RootFolder = System.Environment.SpecialFolder.MyComputer;
            // 
            // btnFindExportPath
            // 
            this.btnFindExportPath.Location = new System.Drawing.Point(424, 69);
            this.btnFindExportPath.Name = "btnFindExportPath";
            this.btnFindExportPath.Size = new System.Drawing.Size(75, 23);
            this.btnFindExportPath.TabIndex = 9;
            this.btnFindExportPath.Text = "Finn...";
            this.btnFindExportPath.UseVisualStyleBackColor = true;
            this.btnFindExportPath.Click += new System.EventHandler(this.btnFindExportPath_Click);
            // 
            // chkShowResults
            // 
            this.chkShowResults.AutoSize = true;
            this.chkShowResults.Location = new System.Drawing.Point(337, 52);
            this.chkShowResults.Name = "chkShowResults";
            this.chkShowResults.Size = new System.Drawing.Size(86, 17);
            this.chkShowResults.TabIndex = 10;
            this.chkShowResults.Text = "Vis resultater";
            this.chkShowResults.UseVisualStyleBackColor = true;
            this.chkShowResults.CheckedChanged += new System.EventHandler(this.chkShowResults_CheckedChanged);
            // 
            // lblNumRowsFoundLabel
            // 
            this.lblNumRowsFoundLabel.AutoSize = true;
            this.lblNumRowsFoundLabel.Location = new System.Drawing.Point(149, 56);
            this.lblNumRowsFoundLabel.Name = "lblNumRowsFoundLabel";
            this.lblNumRowsFoundLabel.Size = new System.Drawing.Size(63, 13);
            this.lblNumRowsFoundLabel.TabIndex = 11;
            this.lblNumRowsFoundLabel.Text = "Antall rader:";
            // 
            // lblNumRowsFound
            // 
            this.lblNumRowsFound.AutoSize = true;
            this.lblNumRowsFound.Location = new System.Drawing.Point(218, 56);
            this.lblNumRowsFound.Name = "lblNumRowsFound";
            this.lblNumRowsFound.Size = new System.Drawing.Size(0, 13);
            this.lblNumRowsFound.TabIndex = 12;
            // 
            // progressBarConversion
            // 
            this.progressBarConversion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBarConversion.Location = new System.Drawing.Point(11, 136);
            this.progressBarConversion.Name = "progressBarConversion";
            this.progressBarConversion.Size = new System.Drawing.Size(487, 15);
            this.progressBarConversion.TabIndex = 13;
            this.progressBarConversion.Visible = false;
            // 
            // lblCompanyFilter
            // 
            this.lblCompanyFilter.AutoSize = true;
            this.lblCompanyFilter.Location = new System.Drawing.Point(12, 95);
            this.lblCompanyFilter.Name = "lblCompanyFilter";
            this.lblCompanyFilter.Size = new System.Drawing.Size(54, 13);
            this.lblCompanyFilter.TabIndex = 14;
            this.lblCompanyFilter.Text = "Firmafilter:";
            // 
            // txtCompanyFilter
            // 
            this.txtCompanyFilter.Location = new System.Drawing.Point(13, 111);
            this.txtCompanyFilter.Name = "txtCompanyFilter";
            this.txtCompanyFilter.Size = new System.Drawing.Size(405, 20);
            this.txtCompanyFilter.TabIndex = 15;
            this.txtCompanyFilter.TextChanged += new System.EventHandler(this.txtCompanyFilter_TextChanged);
            // 
            // frmPRMToNewsML
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(510, 156);
            this.Controls.Add(this.txtCompanyFilter);
            this.Controls.Add(this.lblCompanyFilter);
            this.Controls.Add(this.progressBarConversion);
            this.Controls.Add(this.lblNumRowsFound);
            this.Controls.Add(this.lblNumRowsFoundLabel);
            this.Controls.Add(this.chkShowResults);
            this.Controls.Add(this.btnFindExportPath);
            this.Controls.Add(this.lblFilePathExport);
            this.Controls.Add(this.txtFilePathExport);
            this.Controls.Add(this.previewData);
            this.Controls.Add(this.btnConvert);
            this.Controls.Add(this.archiveTo);
            this.Controls.Add(this.lblArchiveTo);
            this.Controls.Add(this.lblArchiveFrom);
            this.Controls.Add(this.archiveFrom);
            this.Name = "frmPRMToNewsML";
            this.Text = "PRM til NewsML";
            this.Load += new System.EventHandler(this.frmPRMToNewsML_Load);
            ((System.ComponentModel.ISupportInitialize)(this.previewData)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker archiveFrom;
        private System.Windows.Forms.Label lblArchiveFrom;
        private System.Windows.Forms.Label lblArchiveTo;
        private System.Windows.Forms.DateTimePicker archiveTo;
        private System.Windows.Forms.Button btnConvert;
        private System.Windows.Forms.DataGridView previewData;
        private System.Windows.Forms.TextBox txtFilePathExport;
        private System.Windows.Forms.Label lblFilePathExport;
        private System.Windows.Forms.FolderBrowserDialog findExportPath;
        private System.Windows.Forms.Button btnFindExportPath;
        private System.Windows.Forms.CheckBox chkShowResults;
        private System.Windows.Forms.Label lblNumRowsFoundLabel;
        private System.Windows.Forms.Label lblNumRowsFound;
        private System.Windows.Forms.ProgressBar progressBarConversion;
        private System.Windows.Forms.Label lblCompanyFilter;
        private System.Windows.Forms.TextBox txtCompanyFilter;

    }
}

