using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ConvertPRMArchiveToNewsML
{
    public partial class frmPRMToNewsML : Form
    {
        private System.Configuration.AppSettingsReader applicationConfiguration;
        private System.Data.DataSet archiveData = null;
        private System.Data.OleDb.OleDbDataReader archiveReader = null;

        public frmPRMToNewsML()
        {
            InitializeComponent();
            applicationConfiguration = new System.Configuration.AppSettingsReader();
        }

        private void frmPRMToNewsML_Load(object sender, EventArgs e)
        {
            txtFilePathExport.Text = applicationConfiguration.GetValue("Path_Default_Output", string.Empty.GetType()).ToString();
        }

        private void btnConvert_Click(object sender, EventArgs e)
        {
            if (txtFilePathExport.Text == "")
            {
                System.Windows.Forms.MessageBox.Show("Sti � skrive konverterte filer til m� angis", "Filomr�de eksport mangler", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
            }
            else
            {
                if (chkShowResults.Checked && previewData.SelectedRows.Count == 0)
                {
                    System.Windows.Forms.MessageBox.Show("Velg rader som skal konverteres, ellers vil ingenting bli konvertert", "Ingen rader valgt", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
                }
                else
                {
                    try
                    {
                        DBFunctions.DBFunctions dbClass = new DBFunctions.DBFunctions();
                        archiveReader = dbClass.getSQLReader(dbClass.buildPRMArchiveSelectionQuery(archiveFrom.Value, archiveTo.Value, txtCompanyFilter.Text));
                        ConversionFunctions.ConversionFunctions conversionClass = new ConversionFunctions.ConversionFunctions();
                        System.Xml.XmlDocument articleXMLDocument = null;
                        string pathToXSLTFile = Convert.ToString(applicationConfiguration.GetValue("Path_XSLT_NITF32ToNewsML", string.Empty.GetType()));
                        int i = 0;
                        progressBarConversion.Visible = true;
                        progressBarConversion.Maximum = Convert.ToInt32(lblNumRowsFound.Text);
                        progressBarConversion.Step = 1;
                        for (i = 0; (chkShowResults.Checked && i < previewData.SelectedRows.Count) || (!chkShowResults.Checked && archiveReader.HasRows && archiveReader.Read()); i++)
                        {
                            articleXMLDocument = new System.Xml.XmlDocument();
                            if (chkShowResults.Checked)
                            {
                                articleXMLDocument.LoadXml(previewData.SelectedRows[i].Cells["ArticleXML"].Value.ToString());
                            }
                            else
                            {
                                articleXMLDocument.LoadXml(archiveReader.GetString(0));
                            }
                            int lastIndexOfPathDelimiter = articleXMLDocument["nitf"]["head"].SelectSingleNode("meta[@name='filename']").Attributes["content"].Value.LastIndexOf("\\");
                            if (lastIndexOfPathDelimiter < 0)
                            {
                                lastIndexOfPathDelimiter = 0;
                            }
                            //string outFileName = txtFilePathExport.Text + "\\" + articleXMLDocument["nitf"]["head"].SelectSingleNode("meta[@name='subject']").Attributes["content"].Value + "_" + articleXMLDocument["nitf"]["head"].SelectSingleNode("meta[@name='filename']").Attributes["content"].Value.Substring(lastIndexOfPathDelimiter);
                            string outFileName;
                            System.Guid filePathGUID = createGUIDSubFolder(txtFilePathExport.Text);
                            if (filePathGUID == System.Guid.Empty)
                            {
                                outFileName = txtFilePathExport.Text + "\\" + articleXMLDocument["nitf"]["head"].SelectSingleNode("meta[@name='filename']").Attributes["content"].Value.Substring(lastIndexOfPathDelimiter);
                            }
                            else
                            {
                                outFileName = txtFilePathExport.Text + "\\" + filePathGUID.ToString() + "\\" + articleXMLDocument["nitf"]["head"].SelectSingleNode("meta[@name='filename']").Attributes["content"].Value.Substring(lastIndexOfPathDelimiter);
                            }
                            if (chkShowResults.Checked)
                            {
                                conversionClass.transformArticle(previewData.SelectedRows[i].Cells["ArticleXML"].Value.ToString(), pathToXSLTFile, outFileName);
                            }
                            else
                            {
                                conversionClass.transformArticle(archiveReader.GetString(0), pathToXSLTFile, outFileName);
                            }
                            articleXMLDocument.RemoveAll();
                            articleXMLDocument = null;
                            progressBarConversion.PerformStep();
                        }
                        archiveReader.Close();
                        progressBarConversion.Value = 0;
                        progressBarConversion.Visible = false;
                        System.Windows.Forms.MessageBox.Show("Totalt ble " + i.ToString() + " poster konvertert.", "Konvertering ferdig");
                    }
                    catch (System.Exception generalException)
                    {
                        string errorMessage = generalException.Message;
                        if (generalException.InnerException != null)
                        {
                            errorMessage += "\r\nInner exception: " + generalException.InnerException.Message;
                        }
                        System.Windows.Forms.MessageBox.Show(errorMessage, "Exception occured in function frmPRMToNewsML.btnConvert_Click()", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void previewData_SelectionChanged(object sender, EventArgs e)
        {
        }

        private void archiveFrom_CloseUp(object sender, EventArgs e)
        {
            try
            {
                if (chkShowResults.Checked)
                {
                    if (archiveReader != null)
                    {
                        archiveReader.Close();
                    }
                    fillArchiveSet();
                    previewData.DataSource = archiveData.Tables[0];
                }
                else
                {
                    if (archiveData != null)
                    {
                        archiveData.Clear();
                    }
                    initializeDataReader();
                }
            }
            catch (System.Exception generalException)
            {
                string errorMessage = generalException.Message;
                if (generalException.InnerException != null)
                {
                    errorMessage += "\r\nInner exception: " + generalException.InnerException.Message;
                }
                System.Windows.Forms.MessageBox.Show(errorMessage, "Exception occured in function frmPRMToNewsML.archiveFrom_CloseUp()", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
            }
        }

        private void archiveTo_CloseUp(object sender, EventArgs e)
        {
            try
            {
                if (chkShowResults.Checked)
                {
                    if (archiveReader != null)
                    {
                        archiveReader.Close();
                    }
                    fillArchiveSet();
                    previewData.DataSource = archiveData.Tables[0];
                }
                else
                {
                    if (archiveData != null)
                    {
                        archiveData.Clear();
                    }
                    initializeDataReader();
                }
            }
            catch (System.Exception generalException)
            {
                string errorMessage = generalException.Message;
                if (generalException.InnerException != null)
                {
                    errorMessage += "\r\nInner exception: " + generalException.InnerException.Message;
                }
                System.Windows.Forms.MessageBox.Show(errorMessage, "Exception occured in function frmPRMToNewsML.archiveTo_CloseUp()", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
            }
        }

        private void fillArchiveSet()
        {
            if (archiveData != null)
            {
                archiveData.Clear();
            }
            DBFunctions.DBFunctions dbClass = new DBFunctions.DBFunctions();
            archiveData = dbClass.getSQLDataSet(dbClass.buildPRMArchiveSelectionQuery(archiveFrom.Value, archiveTo.Value, txtCompanyFilter.Text));
            lblNumRowsFound.Text = archiveData.Tables[0].Rows.Count.ToString();
        }

        private void initializeDataReader()
        {
            DBFunctions.DBFunctions dbClass = new DBFunctions.DBFunctions();
            archiveReader = dbClass.getSQLReader(dbClass.buildPRMArchiveSelectionCountQuery(archiveFrom.Value, archiveTo.Value, txtCompanyFilter.Text));
            if (archiveReader.Read())
            {
                lblNumRowsFound.Text = archiveReader.GetInt32(0).ToString();
            }
            archiveReader.Close();
        }

        private void btnFindExportPath_Click(object sender, EventArgs e)
        {
            if (findExportPath.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txtFilePathExport.Text = findExportPath.SelectedPath;
            }
        }

        private void chkShowResults_CheckedChanged(object sender, EventArgs e)
        {
            if (chkShowResults.Checked)
            {
                this.Height += 600;
                this.Width += 400;
                previewData.Visible = true;
                if (archiveReader != null)
                {
                    archiveReader.Close();
                }
                fillArchiveSet();
                previewData.DataSource = archiveData.Tables[0];
                previewData.Columns[0].Width = 142;
                previewData.Columns[2].Width = 600;
            }
            else
            {
                previewData.DataSource = null;
                if (archiveData != null)
                {
                    archiveData.Clear();
                }
                previewData.Visible = false;
                this.Height -= 600;
                this.Width -= 400;
                initializeDataReader();
            }
        }

        private System.Guid createGUIDSubFolder(string folderPath)
        {
            try
            {
                System.Guid newGUID = System.Guid.NewGuid();
                System.IO.Directory.CreateDirectory(folderPath + "\\" + newGUID.ToString());
                return newGUID;
            }
            catch (System.IO.IOException IOException)
            {
                System.Windows.Forms.MessageBox.Show("Error creating GUID subfolder in " + folderPath + ".\r\nError: " + IOException.Message);
                return System.Guid.Empty;
            }
            catch (System.Exception Exception)
            {
                System.Windows.Forms.MessageBox.Show("Error creating GUID subfolder in " + folderPath + ".\r\nError: " + Exception.Message);
                return System.Guid.Empty;
            }
        }

        private void txtCompanyFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (chkShowResults.Checked)
                {
                    if (archiveReader != null)
                    {
                        archiveReader.Close();
                    }
                    fillArchiveSet();
                    previewData.DataSource = archiveData.Tables[0];
                }
                else
                {
                    if (archiveData != null)
                    {
                        archiveData.Clear();
                    }
                    initializeDataReader();
                }
            }
            catch (System.Exception generalException)
            {
                string errorMessage = generalException.Message;
                if (generalException.InnerException != null)
                {
                    errorMessage += "\r\nInner exception: " + generalException.InnerException.Message;
                }
                System.Windows.Forms.MessageBox.Show(errorMessage, "Exception occured in function frmPRMToNewsML.txtCompanyFilter_TextChanged()", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
            }
        }
    }
}