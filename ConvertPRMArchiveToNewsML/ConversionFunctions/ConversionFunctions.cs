using System;
using System.Collections.Generic;
using System.Text;

namespace ConversionFunctions
{
    public class ConversionFunctions
    {
        public ConversionFunctions()
        {
        }

        public System.Xml.XmlDocument transformArticle(string xmlAsText, string XSLTFilePathAndName, string outFilePathAnName)
        {
            System.Xml.XmlDocument convertXML = new System.Xml.XmlDocument();
            convertXML.LoadXml(xmlAsText);
            // Debug save of XML
            // convertXML.Save("C:\\TestXML.xml");
            System.Xml.Xsl.XslCompiledTransform xslTransformationObject = new System.Xml.Xsl.XslCompiledTransform();
            //System.IO.TextReader xmlStyleSheetReader = System.IO.File.OpenText(XSLTFilePathAndName);
            System.IO.Stream XSLTReader = System.IO.File.Open(XSLTFilePathAndName, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read);
            System.Xml.XmlReaderSettings xmlSettings = new System.Xml.XmlReaderSettings();
            xmlSettings.ValidationType = System.Xml.ValidationType.Auto;
            xmlSettings.ConformanceLevel = System.Xml.ConformanceLevel.Auto;
            xmlSettings.IgnoreComments = true;
            xmlSettings.IgnoreWhitespace = false;
            System.Xml.XmlReader xmlReader = System.Xml.XmlReader.Create(XSLTReader, xmlSettings);
            System.Xml.XmlWriterSettings xmlOutSettings = new System.Xml.XmlWriterSettings();
            xmlOutSettings.ConformanceLevel = System.Xml.ConformanceLevel.Auto;
            xmlOutSettings.CloseOutput = true;
            //xmlOutSettings.OutputMethod = System.Xml.XmlOutputMethod.Xml;
            xslTransformationObject.Load(xmlReader);
            System.IO.Stream xmlStream = new System.IO.MemoryStream();
            convertXML.Save(xmlStream);
            xmlStream.Flush();
            xmlStream.Seek(0, System.IO.SeekOrigin.Begin);
            xmlReader = System.Xml.XmlReader.Create(xmlStream);
            System.Xml.XmlWriter xmlWriter = System.Xml.XmlWriter.Create(outFilePathAnName, xmlOutSettings);
            System.Xml.Xsl.XsltArgumentList xslArguments = new System.Xml.Xsl.XsltArgumentList();
            xslArguments.AddParam("filename", "", outFilePathAnName);
            xslTransformationObject.Transform(xmlReader, xslArguments, xmlWriter);
            xmlWriter.Close();
            XSLTReader.Close();
            xmlReader.Close();
            xmlStream.Close();
            convertXML.RemoveAll();
            convertXML = null;
            //xmlStyleSheetReader.Close();
            xslTransformationObject = null;
            System.Xml.XmlDocument convertedResult = new System.Xml.XmlDocument();
            convertedResult.Load(outFilePathAnName);
            return convertedResult;
        }
    }
}
