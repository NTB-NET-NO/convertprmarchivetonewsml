<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="UTF-8" indent="yes" cdata-section-elements="DataContent"/>

<xsl:param name="filename">
</xsl:param>

<xsl:param name="iptc_seq">
</xsl:param>

<xsl:param name="duidCounter" select="100001">
</xsl:param>

<xsl:variable name="timestamp" select="/nitf/head/pubdata/@date.publication"/>

<xsl:variable name="date-issue.norm">
	<xsl:value-of select="substring($timestamp, 1, 4)" />
	<xsl:value-of select="substring($timestamp, 5, 2)" />
	<xsl:value-of select="substring($timestamp, 7, 2)" />
		<xsl:text>T</xsl:text>
	<xsl:value-of select="substring($timestamp, 10, 2)" />
	<xsl:value-of select="substring($timestamp, 12, 2)" />
	<xsl:value-of select="substring($timestamp, 14, 2)" />
		<xsl:text>+0100</xsl:text>
</xsl:variable>

<xsl:variable name="dateId">
	<xsl:value-of select="substring($timestamp, 1, 4)" />
	<xsl:value-of select="substring($timestamp, 5, 2)" />
	<xsl:value-of select="substring($timestamp, 7, 2)" />
</xsl:variable>

<xsl:variable name="bodyContentLength">
	<xsl:value-of select="string-length(/nitf/body/body.content)"/>
</xsl:variable>

<xsl:variable name="leadLength">
	<xsl:choose>
		<xsl:when test="/nitf/body/body.content/p[@lede='true' or @class='lead']">
			<xsl:value-of select="string-length(.)"/>
		</xsl:when>
	</xsl:choose>
</xsl:variable>

<xsl:template match="nitf">
	<NewsML>
		<Catalog Href="http://belga.be/dtd/PRSCatalog.xml"/>
		<NewsEnvelope>
			<TransmissionId Repeat="0">
				<xsl:text>1000</xsl:text>
			</TransmissionId>
			<SentFrom>
				<Party>
					<xsl:attribute name="FormalName">
						<xsl:text>NTB Info</xsl:text>
						<!-- <xsl:value-of select="head/meta[@name='NTBDistribusjonskode']/@content"/> -->
					</xsl:attribute>
					<Property>
						<xsl:attribute name="FormalName">
							<xsl:text>webserviceurl</xsl:text>
						</xsl:attribute>
						<xsl:attribute name="Value">
							<xsl:text>webserviceurl.vlaanderen.be</xsl:text>
						</xsl:attribute>
					</Property>
					<!-- <xsl:attribute name="FormalName">
						<xsl:value-of select="head/docdata/doc-id/@regsrc"/> -->
						<!--<xsl:value-of select="head/meta[@name='NTBKilde']/@content"/>-->
					<!-- </xsl:attribute> -->
				</Party>
			</SentFrom>
			<SentTo>
				<Party>
					<xsl:attribute name="FormalName">
						<xsl:text>NTB website</xsl:text>
						<!-- <xsl:value-of select="head/meta[@name='NTBDistribusjonskode']/@content"/> -->
					</xsl:attribute>
					<Property>
						<xsl:attribute name="FormalName">
							<xsl:text>Website</xsl:text>
						</xsl:attribute>
						<xsl:attribute name="Value">
							<xsl:text>http://ntb.com</xsl:text>
						</xsl:attribute>
					</Property>
					<!-- <Property FormalName="Language" Value="no-NO"/>
						<Property FormalName="Email">
							<xsl:attribute name="Value">
							</xsl:attribute>
						</Property> -->
				</Party>
				<!-- <Party>
					<xsl:attribute name="FormalName">
						<xsl:value-of select="head/meta[@name='NTBKanal']/@content"/>
					</xsl:attribute>
				</Party> -->
			</SentTo>
			<DateAndTime>
				<xsl:value-of select="$date-issue.norm"/>
			</DateAndTime>
			<NewsService>
				<xsl:attribute name="FormalName">
					<xsl:value-of select="head/meta[@name='NTBTjeneste']/@content"/>
				</xsl:attribute>
			</NewsService>
			<NewsProduct>
				<xsl:attribute name="FormalName">
					<xsl:value-of select="head/meta[@name='foldername']/@content"/>
				</xsl:attribute>
			</NewsProduct>
			<Priority>
				<xsl:attribute name="FormalName">
					<xsl:value-of select="head/meta[@name='NTBPrioritet']/@content"/>
				</xsl:attribute>
			</Priority>
			</NewsEnvelope>
		<NewsItem>
			<Identification>
				<NewsIdentifier>
					<ProviderId>www.ntb.no</ProviderId>
					<DateId>
						<xsl:value-of select="$dateId"/>
					</DateId>
					<NewsItemId></NewsItemId>
					<RevisionId PreviousRevision="0" Update="N">
						<xsl:value-of select="head/docdata/du-key/@version"/>
					</RevisionId>
					<PublicIdentifier>
						<xsl:text>urn:newsml:</xsl:text>
						<xsl:value-of select="head/docdata/doc-id/@regsrc"/>
						<xsl:text>:</xsl:text>
						<xsl:value-of select="head/docdata/doc-id/@id-string"/>
					</PublicIdentifier>
				</NewsIdentifier>
			</Identification>
			<NewsManagement>
				<NewsItemType FormalName="News" Scheme="IptcNewsItemType"/>
				<FirstCreated>
					<xsl:value-of select="$date-issue.norm"/>
				</FirstCreated>
				<ThisRevisionCreated>
					<xsl:value-of select="$date-issue.norm"/>
				</ThisRevisionCreated>
				<Status FormalName="USABLE" Scheme="IptcStatus"/>
				<Urgency>
					<xsl:attribute name="FormalName">
						<xsl:value-of select="head/docdata/urgency/@ed-urg"/>
					</xsl:attribute>
				</Urgency>
				<Instruction FormalName="pressreleasepublished" Scheme="PRSInstructionCodes"/>
			</NewsManagement>
			<NewsComponent>
				<xsl:attribute name="Duid">
					<xsl:text>d</xsl:text>
					<xsl:call-template name="duidIncrementer">
						<xsl:with-param name="oldDuid" select="$duidCounter"/>
					</xsl:call-template>
				</xsl:attribute>
				<NewsLines>
					<HeadLine>
						<xsl:value-of select="head/title"/>
					</HeadLine>
					<DateLine>
						<xsl:value-of select="$date-issue.norm"/>
					</DateLine>
				</NewsLines>
				<AdministrativeMetadata>
					<FileName>
						<xsl:value-of select="head/meta[@name='filename']/@content"/>
					</FileName>
					<SystemIdentifier>
						<xsl:value-of select="$filename"/>
					</SystemIdentifier>
					<Creator>
						<Party FormalName="">
							<Property FormalName="Language" Value="no-NO"/>
							<Property FormalName="Phone">
								<xsl:attribute name="Value">
									<xsl:text>123456</xsl:text>
								</xsl:attribute>
							</Property>
							<Property FormalName="Email">
								<xsl:attribute name="Value">
									<xsl:text>importerservice@ntbinfo.no</xsl:text>
								</xsl:attribute>
							</Property>
							<Property FormalName="Sector">
								<xsl:attribute name="Value">
									<xsl:text>[ingen]</xsl:text>
								</xsl:attribute>
							</Property>
							<Property FormalName="Organisation">
								<xsl:attribute name="Value">
									<xsl:choose>
										<xsl:when test="head/docdata/doc-id/@regsrc!='NTB'">
											<xsl:value-of select="head/docdata/doc-id/@regsrc"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:text>NTB</xsl:text>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:attribute>
							</Property>
							<Property FormalName="UserGroup">
								<xsl:attribute name="Value">
								</xsl:attribute>
							</Property>
							<Property FormalName="LastName">
								<xsl:attribute name="Value">
									<xsl:text>Service</xsl:text>
								</xsl:attribute>
							</Property>
							<Property FormalName="Website">
								<xsl:attribute name="Value">
									<xsl:text>http://www.ntb.no</xsl:text>
								</xsl:attribute>
							</Property>
							<Property FormalName="FirstName">
								<xsl:attribute name="Value">
									<xsl:text>Importer</xsl:text>
								</xsl:attribute>
							</Property>
							<Property FormalName="Gender">
								<xsl:attribute name="Value">
								</xsl:attribute>
							</Property>
							<Property FormalName="ForOrganisation">
								<xsl:attribute name="Value">
									<xsl:choose>
										<xsl:when test="head/meta[@name='NTBKilde']/@content!='NTB'">
											<xsl:value-of select="head/meta[@name='NTBKilde']/@content"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:text>NTB</xsl:text>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:attribute>
							</Property>
						</Party>
					</Creator>
					<!-- <Contributor>
						<Party FormalName="">
							<Property FormalName="Gender">
								<xsl:attribute name="Value">
								</xsl:attribute>
							</Property>
							<Property FormalName="FirstName">
								<xsl:attribute name="Value">
								</xsl:attribute>
							</Property>
							<Property FormalName="LastName">
								<xsl:attribute name="Value">
								</xsl:attribute>
							</Property>
							<Property FormalName="Phone">
								<xsl:attribute name="Value">
								</xsl:attribute>
							</Property>
							<Property FormalName="Email">
								<xsl:attribute name="Value">
								</xsl:attribute>
							</Property>
						</Party>
					</Contributor> -->
				</AdministrativeMetadata>
				<DescriptiveMetadata>
					<Genre FormalName="Press Release" Scheme="IptcGenre"/>
					<Property FormalName="PressReleaseId">
						<xsl:attribute name="Value">
							<xsl:value-of select="head/meta[@name='NTBIPTCSequence']/@content"/>
						</xsl:attribute>
					</Property>
					<!-- <Property FormalName="Routing">
						<Property FormalName="Name" Value="NTB WIRE"/>
						<Property FormalName="Name" Value="NTB website"/>
						<Property FormalName="Name" Value="NTB Info"/>
					</Property> -->
				</DescriptiveMetadata>
				<NewsComponent xml:lang="no-NO">
					<xsl:attribute name="Duid">
						<xsl:text>d</xsl:text>
						<xsl:call-template name="duidIncrementer">
							<xsl:with-param name="oldDuid" select="$duidCounter + 1"/>
						</xsl:call-template>
					</xsl:attribute>
					<Role FormalName="PRESSCONTENT" Scheme="PRSRole"/>
					<NewsLines>
						<HeadLine>
							<xsl:value-of select="head/title"/>
						</HeadLine>
						<DateLine>
							<xsl:value-of select="$date-issue.norm"/>
						</DateLine>
						<xsl:call-template name="keyword-split">
							<xsl:with-param name="original">
								<xsl:value-of select="head/docdata/key-list/keyword/@key"/>
							</xsl:with-param>
							<xsl:with-param name="substring">
								<xsl:text> </xsl:text>
							</xsl:with-param>
						</xsl:call-template>
					</NewsLines>
					<DescriptiveMetadata>
						<xsl:choose>
							<xsl:when test="head/tobject/tobject.subject">
								<xsl:for-each select="head/tobject/tobject.subject">
									<SubjectCode>
										<xsl:if test="@tobject.subject.type">
											<Subject>
												<xsl:attribute name="FormalName">
													<xsl:value-of select="@tobject.subject.refnum"/>
												</xsl:attribute>
											</Subject>
										</xsl:if>
										<xsl:if test="@tobject.subject.matter">
											<SubjectMatter>
												<xsl:attribute name="FormalName">
													<xsl:value-of select="@tobject.subject.refnum"/>
												</xsl:attribute>
											</SubjectMatter>
										</xsl:if>
									</SubjectCode>
								</xsl:for-each>
							</xsl:when>
							<xsl:otherwise>
								<SubjectCode>
									<Subject>
										<xsl:attribute name="FormalName">
											<xsl:text>04000000</xsl:text>
										</xsl:attribute>
									</Subject>
								</SubjectCode>
							</xsl:otherwise>
						</xsl:choose>
						<DateLineDate><xsl:value-of select="$date-issue.norm"/></DateLineDate>
						<Location HowPresent="Event">
							<Property FormalName="Region">
								<xsl:attribute name="Value">
									<xsl:text>[riksdekkende]</xsl:text>
								</xsl:attribute>
							</Property>
							<xsl:choose>
								<xsl:when test="head/docdata/evloc">
									<xsl:for-each select="head/docdata/evloc">
										<xsl:choose>
											<xsl:when test="@country-dist">
												<Property FormalName="CountryArea">
													<xsl:attribute name="Value">
														<xsl:value-of select="@country-dist"/>
													</xsl:attribute>
												</Property>
											</xsl:when>
											<xsl:otherwise>
												<Property FormalName="Country">
													<xsl:attribute name="Value">
														<xsl:value-of select="@state-prov"/>
													</xsl:attribute>
												</Property>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:for-each>
								</xsl:when>
								<xsl:otherwise>
									<Property FormalName="Country">
										<xsl:attribute name="Value">
											<xsl:text>Norway</xsl:text>
										</xsl:attribute>
									</Property>
								</xsl:otherwise>
							</xsl:choose>
						</Location>
<!--						<Property FormalName="Type">
							<xsl:attribute name="Value">
								<xsl:value-of select="head/tobject/tobject.property/@tobject.property.type"/>
							</xsl:attribute>
						</Property>-->
						<Property FormalName="Sector">
							<xsl:attribute name="Value">
								<xsl:text>[ingen]</xsl:text>
							</xsl:attribute>
						</Property>
					</DescriptiveMetadata>
					<NewsComponent>
						<xsl:attribute name="Duid">
							<xsl:text>d</xsl:text>
							<xsl:call-template name="duidIncrementer">
								<xsl:with-param name="oldDuid" select="$duidCounter + 2"/>
							</xsl:call-template>
						</xsl:attribute>
						<Role FormalName="Headline" Scheme="PRSRole"/>
						<ContentItem>
							<MediaType FormalName="text"/>
							<Format FormalName="html"/>
							<Characteristics>
								<SizeInBytes>
									<xsl:value-of select="string-length(body/body.head/hedline/hl1)" />
								</SizeInBytes>
								<Property FormalName="FileName">
								</Property>
							</Characteristics>
							<DataContent>
								<xsl:value-of select="substring-after(body/body.head/hedline/hl1, concat('PRM: ', head/meta[@name='NTBKilde']/@content, '/ '))"/>
							</DataContent>
						</ContentItem>
					</NewsComponent>
					<xsl:for-each select="body/body.content/p[@lede='true' or @class='lead']">
						<xsl:if test="string-length(normalize-space(.))&gt;0">
							<NewsComponent>
								<Role FormalName="Short" Scheme="PRSRole"/>
								<ContentItem>
									<MediaType FormalName="text"/>
									<Format FormalName="html"/>
									<Characteristics>
										<SizeInBytes>
											<xsl:value-of select="string-length(normalize-space(.))"/>
										</SizeInBytes>
										<Property FormalName="FileName"/>
									</Characteristics>
									<DataContent>
										<xsl:value-of select="."/>
									</DataContent>
								</ContentItem>
							</NewsComponent>
						</xsl:if>
					</xsl:for-each>
					<xsl:if test="string-length(normalize-space(body/body.content))-string-length(normalize-space(body/body.content/p[@lede='true' or @class='lead']))&gt;0">
						<NewsComponent>
							<Role FormalName="Body" Scheme="PRSRole"/>
							<ContentItem>
								<MediaType FormalName="text"/>
								<Format FormalName="html"/>
								<Characteristics>
									<SizeInBytes>
										<xsl:value-of select="string-length(normalize-space(body/body.content))-string-length(normalize-space(body/body.content/p[@lede='true' or @class='lead']))"/>
									</SizeInBytes>
									<Property FormalName="FileName"/>
								</Characteristics>
								<DataContent>
									<xsl:for-each select="body/body.content/p">
										<xsl:choose>
											<xsl:when test="@lede='true' or @class='lede'">
											</xsl:when>
											<xsl:otherwise>
												<xsl:text>&lt;p&gt;</xsl:text>
												<xsl:value-of select="." disable-output-escaping="no"/>
												<xsl:text>&lt;/p&gt;</xsl:text>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:for-each>
								</DataContent>
							</ContentItem>
						</NewsComponent>
					</xsl:if>
					<xsl:if test="count(head/docdata/ed-msg/@info)&gt;0">
						<NewsComponent>
							<Role FormalName="EditorialNote" Scheme="PRSRole"/>
							<ContentItem>
								<MediaType FormalName="text"/>
								<Format FormalName="html"/>
								<Characteristics>
									<SizeInBytes>
										<xsl:value-of select="string-length(normalize-space(head/docdata/ed-msg/@info))"/>
									</SizeInBytes>
									<Property FormalName="FileName"/>
								</Characteristics>
								<DataContent>
									<xsl:for-each select="head/docdata/ed-msg/@info">
										<xsl:value-of select="."/>
									</xsl:for-each>
								</DataContent>
							</ContentItem>
						</NewsComponent>
					</xsl:if>
				</NewsComponent>
				<xsl:for-each select="body/body.content/media[@media-type='image']">
					<NewsComponent>
						<Role FormalName="Picture" Scheme="PRSRole"/>
						<NewsComponent>
							<Role FormalName="HighRes" Scheme="PRSRole"/>
							<ContentItem>
								<xsl:attribute name="HREF">
									<xsl:choose>
										<xsl:when test="@class='prm'">
											<xsl:text>http://193.75.33.34/prm_vedlegg/vedlegg/</xsl:text>
											<xsl:value-of select="media-reference/@alternate-text"/>
											<xsl:text>/</xsl:text>
											<xsl:value-of select="media-reference/@source"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="media-reference/@alternate-text"/>
											<xsl:text>/</xsl:text>
											<xsl:value-of select="media-reference/@source"/>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:attribute>
								<MediaType>
									<xsl:attribute name="FormalName">
										<xsl:text>picture</xsl:text>
									</xsl:attribute>
								</MediaType>
								<Format>
									<xsl:attribute name="FormalName">
										<xsl:value-of select="substring-after(media-reference/@mime-type, '/')"/>
									</xsl:attribute>
								</Format>
								<Characteristics>
								</Characteristics>
							</ContentItem>
						</NewsComponent>
						<NewsComponent>
							<Role FormalName="Caption" Scheme="PRSRole"/>
							<ContentItem>
								<MediaType>
									<xsl:attribute name="FormalName">
										<xsl:text>text</xsl:text>
									</xsl:attribute>
								</MediaType>
								<Format>
									<xsl:attribute name="FormalName">
										<xsl:text>ascii</xsl:text>
									</xsl:attribute>
								</Format>
								<DataContent>
									<xsl:copy-of select="media-caption"/>
								</DataContent>
							</ContentItem>
						</NewsComponent>
					</NewsComponent>
				</xsl:for-each>
				<xsl:for-each select="body/body.content/media[@media-type='document']">
					<NewsComponent>
						<Role FormalName="Binary" Scheme="PRSRole"/>
						<NewsComponent>
							<Role FormalName="Supplementary" Scheme="PRSRole"/>
							<ContentItem>
								<xsl:attribute name="HREF">
									<xsl:choose>
										<xsl:when test="@class='prm'">
											<xsl:text>http://193.75.33.34/prm_vedlegg/vedlegg/</xsl:text>
											<xsl:value-of select="media-reference/@alternate-text"/>
											<xsl:text>/</xsl:text>
											<xsl:value-of select="media-reference/@source"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="media-reference/@alternate-text"/>
											<xsl:text>/</xsl:text>
											<xsl:value-of select="media-reference/@source"/>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:attribute>
								<MediaType>
									<xsl:attribute name="FormalName">
										<xsl:value-of select="media-reference/@name"/>
									</xsl:attribute>
								</MediaType>
								<Format>
									<xsl:attribute name="FormalName">
										<xsl:value-of select="media-reference/@mime-type"/>
									</xsl:attribute>
								</Format>
								<Characteristics>
								</Characteristics>
							</ContentItem>
						</NewsComponent>
						<NewsComponent>
							<Role FormalName="Caption" Scheme="PRSRole"/>
							<ContentItem>
								<MediaType>
									<xsl:attribute name="FormalName">
										<xsl:text>text</xsl:text>
									</xsl:attribute>
								</MediaType>
								<Format>
									<xsl:attribute name="FormalName">
										<xsl:text>ascii</xsl:text>
									</xsl:attribute>
								</Format>
								<DataContent>
									<xsl:copy-of select="media-caption"/>
								</DataContent>
							</ContentItem>
						</NewsComponent>
					</NewsComponent>
				</xsl:for-each>
			</NewsComponent>
		</NewsItem>
	</NewsML>
</xsl:template>

<xsl:template match="head">
	<xsl:copy-of select="title"/>
	<xsl:copy-of select="meta"/>

	<meta name="NTBIPTCSequence">
	<xsl:attribute name="content">
	<xsl:value-of select="$iptc_seq"/>
	</xsl:attribute>
	</meta>

	<xsl:copy-of select="tobject"/>
	<xsl:copy-of select="docdata"/>
	
	<xsl:copy-of select="pubdata"/>
	<xsl:copy-of select="revision-history"/>
	
	
</xsl:template>

<xsl:template name="keyword-split">
	<xsl:param name="original"/>
	<xsl:param name="substring"/>
	<xsl:choose>
		<xsl:when test="contains($original, $substring)">
			<KeywordLine>
				<xsl:value-of select="substring-before($original, $substring)"/>
			</KeywordLine>
		</xsl:when>
		<xsl:otherwise>
			<xsl:if test="string-length($original)&gt;0">
				<KeywordLine>
					<xsl:value-of select="$original"/>
				</KeywordLine>
			</xsl:if>
		</xsl:otherwise>
	</xsl:choose>
	<xsl:choose>
		<xsl:when test="contains($original, $substring)">
			<xsl:choose>
				<xsl:when test="contains(substring-after($original, $substring), $substring)">
					<xsl:call-template name="keyword-split">
						<xsl:with-param name="original">
							<xsl:value-of select="substring-after($original, $substring)"/>
						</xsl:with-param>
						<xsl:with-param name="substring">
							<xsl:value-of select="$substring"/>
						</xsl:with-param>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<KeywordLine>
						<xsl:value-of select="substring-after($original, $substring)"/>
					</KeywordLine>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>
	</xsl:choose>
</xsl:template>

<xsl:template name="duidIncrementer">
	<xsl:param name="oldDuid" />
	<xsl:choose>
		<xsl:when test="NewsComponent[@Duid=$oldDuid]"></xsl:when>
	</xsl:choose>
	<xsl:value-of select="$oldDuid + 1"/>
</xsl:template>

</xsl:stylesheet>